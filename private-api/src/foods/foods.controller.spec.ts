import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Food } from './entities/food.entity';
import { FoodsController } from './foods.controller';
import { FoodsService } from './foods.service';



const food_results = [
  { id: 1, description: 'food description 1', publicationDate: '2021-11-01' },
  { id: 2, description: 'food description s2', publicationDate: '2021-11-02' },
  { id: 3, description: 'food description s3', publicationDate: '2021-11-03' },
  { id: 4, description: 'food description 4', publicationDate: '2021-11-04' },
];

const mockRepository = jest.fn(() => ({
  metadata: {
    columns: [],
    relations: [],
  },
}));

const mockFoodService = {

  create: jest.fn(dto => {
    return {
      id: 1,
      ...dto
    }
  }),

  findAll: jest.fn((search, page, limit) => {
    return food_results.filter(food => food.description.indexOf(search) >= 0).slice(page, limit);
  }),

  findOne: jest.fn((id) => {
    const food_item = food_results.filter(food => food.id == id);
    if( food_item.length ) return food_item[0];
    else return {};
  }),

  update: jest.fn((id, dto) => {
    const food_item = food_results.filter(food => food.id == id);
    return {...(food_item.length ? food_item[0] : {}), ...dto};
  }),

  delete: jest.fn((id) => {
    const food_item = food_results.filter(food => food.id == id);
    return {
      raw: [],
      affected: food_item.length,
    };
  })
}

describe('FoodsController', () => {
  let controller: FoodsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FoodsController],
      providers: [
        FoodsService,
        { provide: getRepositoryToken(Food), useClass: mockRepository },
      ],
    }).overrideProvider(FoodsService).useValue(mockFoodService).compile();

    controller = module.get<FoodsController>(FoodsController);
  });


  it('should be defined', () => {
    expect(controller).toBeDefined();
  });


  it('should create food item', () => {
    const dto = { description: 'food description', publicationDate: '2021-11-01' };

    expect(controller.create(dto)).toEqual({
      id: expect.any(Number),
      ...dto
    });

    expect(mockFoodService.create).toHaveBeenCalledWith(dto);
  });

  it('should get all - with search and pagination', () => {
    expect(mockFoodService.findAll('description s', 0, 2)).toEqual([food_results[1], food_results[2]]);
  });

  it('should get one', () => {
    expect(mockFoodService.findOne(2)).toEqual(food_results[1]);
  });

  it('should update food item', () => {
    const dto = { description: 'food description', publicationDate: '2021-11-01' };
    expect(mockFoodService.update(2, dto)).toEqual( {id:2, ...dto} );
  });

  it('should delete food item', () => {
    expect(mockFoodService.delete(2).affected).toEqual( 1 );
  });



});
