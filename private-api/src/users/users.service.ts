import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getManager } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  create(createUserDto: CreateUserDto) {
    return this.usersRepository.save(createUserDto);
  }

  findAll(skip = 0, take = 25) {
    return this.usersRepository.find({
      skip,
      take,
    });
  }

  findOne(id: number) {
    return this.usersRepository.findOneOrFail(id);
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    await this.usersRepository.update({ id }, updateUserDto);
    return this.usersRepository.findOneOrFail(id);
  }

  remove(id: number) {
    return this.usersRepository.delete({ id });
  }

  
  async mostConsumedNutrient(user_id: number) {
    const query = getManager().createQueryBuilder()
                  .from('user_foods', 'uf')
                  .addSelect('fn.nutrient_id')
                  .addSelect('n.name')
                  .addSelect('n.unit_name')
                  .addSelect('sum(uf.servings_per_week * fn.amount_per_serving)', 'weekly_amount')
                  .innerJoin('food_nutrients', 'fn', 'uf.food_id = fn.food_id')
                  .innerJoin('nutrients', 'n', 'fn.nutrient_id = n.id')
                  .where('uf.user_id = :user_id', {user_id})
                  .groupBy('fn.nutrient_id')
                  .orderBy('weekly_amount', 'DESC');

    return await query.getRawOne();
  }
}
