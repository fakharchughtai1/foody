<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class UserFoodController extends Controller
{
    private $privateApiUrl;

    public function __construct()
    {
        $this->privateApiUrl = env('PRIVATE_API_URL');
    }

    public function foods(\App\Http\Requests\UserFoodRequest $request, int $userId)
    {
        $response = Http::get("{$this->privateApiUrl}/users/{$userId}/foods");
        if( $response->status() != 200 ){
            throw new \ErrorException('Something is wrong');   
        }        
        return $response->json();
    }

    public function food(\App\Http\Requests\UserFoodRequest $request, int $userId, int $foodId)
    { 
        $response = Http::get("{$this->privateApiUrl}/users/{$userId}/foods/{$foodId}");
        if( $response->status() != 200 ){
            throw new \ErrorException('Something is wrong');   
        }

        return $response->json();
    }

    public function deleteFood(\App\Http\Requests\UserFoodRequest $request, int $userId, int $foodId)
    {
        $response = Http::delete("{$this->privateApiUrl}/users/{$userId}/foods/{$foodId}");
        if( $response->status() != 204 ){
            throw new \ErrorException('Something is wrong');   
        }
        return $response->json();
    }

    public function addFood(\App\Http\Requests\UserFoodRequest $request, int $userId, int $foodId)
    {
        $servingsPerWeek = $request->input('servingsPerWeek');
        
        $response = Http::put("{$this->privateApiUrl}/users/{$userId}/foods/{$foodId}", [
            'servingsPerWeek' => $servingsPerWeek
        ]);
        if( $response->status() != 200 ){
            throw new \ErrorException('Something is wrong');   
        }        
        return $response->json();
    }
}
