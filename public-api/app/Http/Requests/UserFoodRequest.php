<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserFoodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function validationData()
    {
        $data = $this->all();
        $data['userId'] = $this->route('userId');
        if (request()->isMethod('PUT') || request()->isMethod('DELETE')) {
            $data['foodId'] = $this->route('foodId');
        }
        else if (request()->isMethod('GET') && $this->route('foodId') ){
            $data['foodId'] = $this->route('foodId');
        }
        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $inputs =  $this->validationData();

        $rules = [
            'userId' => 'required|numeric',
        ];

        if( isset($inputs['foodId']) ) {
            $rules['foodId'] = 'required|numeric';
        }

        if (request()->isMethod('PUT') ) {
            $rules['servingsPerWeek'] = 'required|numeric';
        }
        
        return $rules;
    }
}
